export interface IUser {
  phone: string
  designation: string
  employeeCode: string
  avatar: string
  name: string
  joiningDate: string
  id: string
}
