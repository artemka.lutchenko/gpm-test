import { URL_GET_USER } from '@/constants/urls'
import { IUser } from '@/api'

export async function getUser (): Promise<IUser[]> {
  const fetchResult = await fetch(URL_GET_USER, {
    method: 'GET'
  })

  return fetchResult.json()
}
