import Vue from 'vue'
import Component from 'vue-class-component'

@Component<InstanceType<typeof GPMTabs>>({
  props: {
    tabs: {
      type: Array,
      default: () => ([])
    }
  },
  watch: {
    activeIndex () {
      this.$nextTick(() => {
        this.defineWrapperStyle()
      })
    }
  }
})
export default class GPMTabs extends Vue {
  // Options
  $refs!: {
    'content': HTMLDivElement
  }

  // Props
  readonly tabs!: string[]

  wrapperStyle: { left: number | string, width: number | string } = {
    left: 0,
    width: 0
  }

  activeIndex = 0

  defineWrapperStyle (): void {
    if (!this.$el) return
    const activeElement = this.$el.querySelectorAll('.gpm-tabs__tab')[this.activeIndex]
    const content = this.$refs.content

    if (!activeElement || !content) return

    const coordsActiveElement = activeElement.getBoundingClientRect()
    const coordsContent = content.getBoundingClientRect()

    this.wrapperStyle = {
      left: `${coordsActiveElement.left - coordsContent.left}px`,
      width: `${coordsActiveElement.width}px`
    }
  }

  onClickHandler (idx: number): void {
    this.activeIndex = idx
    this.$emit('change', idx)
  }

  mounted (): void {
    this.defineWrapperStyle()
  }
}
