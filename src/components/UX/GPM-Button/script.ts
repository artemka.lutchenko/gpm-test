import Vue from 'vue'
import Component from 'vue-class-component'

// Components
import GPMIcon from '@/components/UX/GPM-Icon/index.vue'

@Component({
  components: {
    GPMIcon
  },
  props: {
    appendIcon: String,
    prevIcon: String,
    color: String
  }
})
export default class GPMButton extends Vue {
  // Props
  readonly appendIcon!: string
  readonly prevIcon!: string
  readonly color!: string

  // Computed
  get classes (): Record<string, boolean> {
    return {
      [`gpm-button--${this.color}`]: true
    }
  }

  onClickHandler (e: Event): void {
    this.$emit('click', e)
  }
}
