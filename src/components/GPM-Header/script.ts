import Vue from 'vue'
import Component from 'vue-class-component'

// Components
import GPMButton from '@/components/UX/GPM-Button/index.vue'

@Component({
  components: {
    GPMButton
  }
})
export default class GPMHeader extends Vue {}
