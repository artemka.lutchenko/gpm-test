module.exports = {
  css: {
    sourceMap: true,
    loaderOptions: {
      sass: {
        prependData: `
          @import "~@/assets/scss/_vars.scss";
        `
      }
    }
  },

  chainWebpack: config => {
    config.module
      .rule('svg-sprite')
      .use('svgo-loader')
      .loader('svgo-loader')

    config.module
      .rule('svg-sprite')
      .use('svgo-loader')
      .loader('svgo-loader')
      .tap(options => {
        return {
          plugins: [
            { collapseGroups: true },
            { removeUselessStrokeAndFill: { removeNone: true } },
            { convertColors: { currentColor: true } }
          ]
        }
      })
  }
}
